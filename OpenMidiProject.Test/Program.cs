﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using OpenMidiProject.Midi.IO;

namespace OpenMidiProject.Test
{
	class Program
	{
		private static void Main(string[] args)
		{
			try
			{
				Console.WriteLine("アプリケーションは、{0} ビットとして起動しています。", IntPtr.Size * 8);

				Console.WriteLine("利用可能な MIDI 出力デバイス数: " + MidiOut.Count);
				var devices = MidiOut.MidiDevices;
				Console.WriteLine(string.Join("\n", devices.Select((s, i) => i + ". " + s.Name + " [" + s.Technorogy + "]")));

				int selectId = -1;
				while (true)
				{
					Console.Write("MIDI 出力デバイスを選択 (-1: MIDI マッパー): ");

					if (!int.TryParse(Console.ReadLine(), out selectId))
						selectId = -1;

					break;
				}

				using (var midiOut = new MidiOut(selectId))
				{
					Console.WriteLine(midiOut.DeviceInfo);

					midiOut.SendMessage(new[] { ((byte)(0x90 + 0)), (byte)60, (byte)127 });
					Thread.Sleep(1000);
					midiOut.SendMessage(new[] {((byte)(0x80 + 0)), (byte)60, (byte)64});
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine("!!Exception!! {0}: {1}", ex.GetType(), ex.Message);
			}
		}
	}
}
