﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OpenMidiProject.Midi.Api
{
	/// <summary>
	/// MIDI 出力デバイスから送られたコールバック メッセージを定義します。
	/// </summary>
	public enum MidiOutMessage : uint
	{
		/// <summary>
		/// MIDI 出力デバイスをオープンしました。
		/// </summary>
		Open = 0x3c7,
		/// <summary>
		/// MIDI 出力デバイスをクローズしました。
		/// </summary>
		Close = 0x3c8,
		/// <summary>
		/// 送信されたメッセージを処理しました。
		/// </summary>
		Done = 0x3c9
	}
}
