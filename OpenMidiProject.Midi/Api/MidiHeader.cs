﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace OpenMidiProject.Midi.Api
{
	[StructLayout(LayoutKind.Sequential)]
	internal struct MidiHeader
	{
		[MarshalAs(UnmanagedType.SysUInt)]
		public IntPtr lpData;

		[MarshalAs(UnmanagedType.U4)]
		public uint dwBufferLength;

		[MarshalAs(UnmanagedType.U4)]
		public uint dwBytesRecorded;

		[MarshalAs(UnmanagedType.U8)]
		public ulong dwUser;

		[MarshalAs(UnmanagedType.U4)]
		public MidiHeaderFlags dwFlags;

		[MarshalAs(UnmanagedType.SysUInt)]
		public IntPtr lpNext;

		[MarshalAs(UnmanagedType.U8)]
		public ulong reserved;

		[MarshalAs(UnmanagedType.U4)]
		public uint dwOffset;

		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
		public ulong[] dwReserved;
	}
}
