﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using OpenMidiProject.Midi.IO;

namespace OpenMidiProject.Midi.Api
{
	internal static class MidiOutApi
	{
		#region P/Invoke Windows

#if WINDOWS

		[DllImport("winmm.dll", EntryPoint = "midiOutGetNumDevs")]
		public static extern uint MidiOutGetNumDevs();

		[DllImport("winmm.dll", EntryPoint = "midiOutGetDevCapsW")]
		public static extern MmResult MidiOutGetDevCapsW([MarshalAs(UnmanagedType.U4)] uint uDeviceId,
														ref MidiOutCaps pMidiOutCaps, [MarshalAs(UnmanagedType.U4)] uint cbMidiOutCaps);

		[DllImport("winmm.dll", EntryPoint = "midiOutGetDevCapsW")]
		public static extern MmResult MidiOutGetDevCapsWLong([MarshalAs(UnmanagedType.U8)] ulong uDeviceId,
															ref MidiOutCaps pMidiOutCaps, [MarshalAs(UnmanagedType.U4)] uint cbMidiOutCaps);

		[DllImport("winmm.dll", EntryPoint = "midiOutOpen")]
		public static extern MmResult MidiOutOpen(ref IntPtr phmo, [MarshalAs(UnmanagedType.U4)] uint uDeviceId,
												[MarshalAs(UnmanagedType.FunctionPtr)] Delegate dwCallBack, [MarshalAs(UnmanagedType.U4)] uint dwInstance,
												[MarshalAs(UnmanagedType.U4)] MidiPortOpenFlags dwFlags);

		[DllImport("winmm.dll", EntryPoint = "midiOutOpen")]
		public static extern MmResult MidiOutOpenLong(ref IntPtr phmo, [MarshalAs(UnmanagedType.U8)] ulong uDeviceId,
													[MarshalAs(UnmanagedType.FunctionPtr)] Delegate dwCallBack, [MarshalAs(UnmanagedType.U8)] ulong dwInstance,
													[MarshalAs(UnmanagedType.U4)] MidiPortOpenFlags dwFlags);

		[DllImport("winmm.dll", EntryPoint = "midiOutClose")]
		public static extern MmResult MidiOutClose(IntPtr phmo);

		[DllImport("winmm.dll", EntryPoint = "midiOutReset")]
		public static extern MmResult MidiOutReset(IntPtr phmo);

		[DllImport("winmm.dll", EntryPoint = "midiOutGetErrorTextW")]
		public static extern uint MidiOutGetErrorText([MarshalAs(UnmanagedType.U4)]MmResult result, [MarshalAs(UnmanagedType.LPWStr)] StringBuilder pszText,
													uint cchText);

		[DllImport("winmm.dll", EntryPoint = "midiOutShortMsg")]
		public static extern MmResult MidiOutShortMsg(IntPtr phmo, uint dwMsg);

		[DllImport("winmm.dll", EntryPoint = "midiOutLongMsg")]
		public static extern MmResult MidiOutLongMsg(IntPtr phmo, ref MidiHeader lpMidiOutHdr, uint cbMidiOutHdr);

		[DllImport("winmm.dll", EntryPoint = "midiOutPrepareHeader")]
		public static extern MmResult MidiOutPrepareHeader(IntPtr phmo, ref MidiHeader pmh, uint cbmh);

		[DllImport("winmm.dll", EntryPoint = "midiOutUnprepareHeader")]
		public static extern MmResult MidiOutUnprepareHeader(IntPtr phmo, ref MidiHeader pmh, uint cbmh);

#endif

		#endregion
	}
}
