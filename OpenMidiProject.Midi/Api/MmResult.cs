﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OpenMidiProject.Midi.Api
{
	/// <summary>
	/// 
	/// </summary>
	public enum MmResult
	{
		/// <summary>
		/// 正常に終了しました。
		/// </summary>
		NoError = 0,
		/// <summary>
		/// デバイス ID が無効です。
		/// </summary>
		BadDeviceId = 2,
		/// <summary>
		/// ドライバーを有効にできませんでした。
		/// </summary>
		NotEnabled = 3,
		/// <summary>
		/// このデバイスのリソースは、既に割り当てられています。
		/// </summary>
		Allocated = 4,
		/// <summary>
		/// デバイス ハンドルが無効です。
		/// </summary>
		InvalidHandle = 5,
		/// <summary>
		/// ドライバーがインストールされていません。
		/// </summary>
		NoDriver = 6,
		/// <summary>
		/// メモリの確保またはロックに失敗しました。
		/// </summary>
		NoMemory = 7,
		/// <summary>
		/// 指定されたポインターまたは構造体は無効です。
		/// </summary>
		InvalidParameter = 11,
		/// <summary>
		/// 利用可能な MIDI ポートが見つかりません。
		/// </summary>
		NoDevice=68
	}
}
