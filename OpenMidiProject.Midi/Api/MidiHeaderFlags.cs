﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OpenMidiProject.Midi.Api
{
	[Flags]
	public enum MidiHeaderFlags
	{
		None = 0,
		Done = 1,
		Prepared = 2,
		InQueue = 4
	}
}
