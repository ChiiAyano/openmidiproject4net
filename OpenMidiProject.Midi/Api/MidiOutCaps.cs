﻿using System.Runtime.InteropServices;

namespace OpenMidiProject.Midi.Api
{
	[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
	internal struct MidiOutCaps
	{
		public ushort wMid;
		public ushort wPid;
		public uint vDriverVersion;

		[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
		public string szPname;

		public ushort wTechnorogy;
		public ushort wVoices;
		public ushort wNotes;
		public ushort wChannelMask;
		public uint dwSupport;
	}
}
