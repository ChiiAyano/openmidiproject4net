﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OpenMidiProject.Midi.IO
{
	/// <summary>
	/// MIDI デバイスが使用できる機能を定義します。
	/// </summary>
	[Flags]
	public enum MidiDeviceCapability
	{
		/// <summary>
		/// ボリューム コントロールが利用可能です。
		/// </summary>
		Volume = 1,
		/// <summary>
		/// 左右独立したボリューム コントロールが利用可能です。
		/// </summary>
		LrVolume = 2,
		/// <summary>
		/// パッチ キャッシュが利用可能です。
		/// </summary>
		Cache = 4,
		/// <summary>
		/// MIDI ストリームをサポートします。
		/// </summary>
		Stream = 8
	}
}
