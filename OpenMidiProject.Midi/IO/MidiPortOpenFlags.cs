﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OpenMidiProject.Midi.IO
{
	/// <summary>
	/// 
	/// </summary>
	public enum MidiPortOpenFlags
	{
		/// <summary>
		/// 
		/// </summary>
		NoCallback = 0,
		/// <summary>
		/// 
		/// </summary>
		WindowCallback = 0x10000,
		/// <summary>
		/// 
		/// </summary>
		ThreadCallback = 0x20000,
		/// <summary>
		/// 
		/// </summary>
		FunctionCallback = 0x30000,
		/// <summary>
		/// 
		/// </summary>
		EventHandle = 0x50000
	}
}
