﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenMidiProject.Midi.Api;

namespace OpenMidiProject.Midi.IO
{
	/// <summary>
	/// MIDI 出力デバイスについて取得します。
	/// </summary>
	public class MidiOutDeviceInfo
	{
		/// <summary>
		/// MIDI 出力デバイス名を取得します。
		/// </summary>
		public string Name { get; private set; }
		/// <summary>
		/// 最大ボイス数を取得します。
		/// </summary>
		public int MaxVoices { get; private set; }
		/// <summary>
		/// 最大同時発音数を取得します。
		/// </summary>
		public int MaxNotes { get; private set; }
		/// <summary>
		/// MIDI 出力デバイスのデバイス ドライバーのバージョンを取得します。
		/// </summary>
		public Version Version { get; private set; }
		/// <summary>
		/// MIDI 出力デバイスの種類を取得します。
		/// </summary>
		public MidiDeviceType Technorogy { get; private set; }
		/// <summary>
		/// MIDI 出力デバイスが利用できる機能を取得します。
		/// </summary>
		public MidiDeviceCapability Support { get; private set; }
		/// <summary>
		/// チャンネル マスクを取得します。
		/// </summary>
		public bool[] ChannelMask { get; private set; }

		internal MidiOutDeviceInfo(MidiOutCaps midiOutCaps)
		{
			this.Name = midiOutCaps.szPname;
			this.ChannelMask = GetChannelMask(midiOutCaps.wChannelMask);
			this.MaxNotes = midiOutCaps.wNotes;
			this.MaxVoices = midiOutCaps.wVoices;
			this.Technorogy = (MidiDeviceType)midiOutCaps.wTechnorogy;
			this.Support = (MidiDeviceCapability)midiOutCaps.dwSupport;
			this.Version = GetDeviceVersion(midiOutCaps.vDriverVersion);
		}

		private bool[] GetChannelMask(ushort chMask)
		{
			var mask = Enumerable.Range(0, 16)
								.Select(s => (chMask & (1 << s)) != 0);

			return mask.ToArray();
		}

		private Version GetDeviceVersion(uint devVersion)
		{
			return new Version(((int)devVersion >> 8), ((int)devVersion >> 0xff));
		}

		public override string ToString()
		{
			return this.Name + " [" + this.Technorogy + "]";
		}
	}
}
