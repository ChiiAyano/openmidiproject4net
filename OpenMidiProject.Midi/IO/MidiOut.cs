﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using OpenMidiProject.Midi.Api;

namespace OpenMidiProject.Midi.IO
{
	public class MidiOut:IDisposable
	{
		#region 静的

		/// <summary>
		/// 利用可能な MIDI 出力デバイスの数を取得します。
		/// </summary>
		public static int Count
		{
			get { return (int)MidiOutApi.MidiOutGetNumDevs(); }
		}

		/// <summary>
		/// 利用可能な MIDI 出力デバイスを列挙します。
		/// </summary>
		public static IEnumerable<MidiOutDeviceInfo> MidiDevices
		{
			get { return Enumerable.Range(0, MidiOut.Count).Select(s => GetMidiOutCaps(s)); }
		}

		private static MidiOutDeviceInfo GetMidiOutCaps(int devCount)
		{
			var midiOutCaps = new MidiOutCaps();

			if (Environment.Is64BitProcess)
				MidiOutApi.MidiOutGetDevCapsWLong(unchecked((uint)devCount), ref midiOutCaps, (uint)Marshal.SizeOf(midiOutCaps))
						.MidiOutThrow();
			else
				MidiOutApi.MidiOutGetDevCapsW(unchecked((uint)devCount), ref midiOutCaps, (uint)Marshal.SizeOf(midiOutCaps)).MidiOutThrow();

			return new MidiOutDeviceInfo(midiOutCaps);
		}

		#endregion


		#region プロパティ

		/// <summary>
		/// このクラスのインスタンスが既に破棄されているか取得します。
		/// </summary>
		public bool IsDisposed { get; private set; }

		public MidiOutDeviceInfo DeviceInfo
		{
			get { return MidiOut.GetMidiOutCaps(this.deviceId); }
		}

		#endregion

		private IntPtr midiOutHandle;
		private int deviceId = -1;
		private MidiHeader[] sysExHeader;

		#region 定数

		private readonly int systemExclusiveNumber = 4;
		private readonly int systemExclusiveMaxSize = 1024;

		#endregion

		#region MIDI Out コールバック

		public delegate void MidiOutProc(IntPtr handle, MidiOutMessage message, IntPtr instance, IntPtr parameter1, IntPtr parameter2);

		private void MidiOutCallback(IntPtr handle, MidiOutMessage wMessage, IntPtr instance, IntPtr param1, IntPtr param2)
		{
			Console.WriteLine("Callback: {0}:{1}:{2}:{3}:{4}", handle, wMessage, instance, param1, param2);

			switch (wMessage)
			{
				case MidiOutMessage.Open:
					break;
				case MidiOutMessage.Close:
					break;
				case MidiOutMessage.Done:
					break;
				default:
					break;
			}
		}

		

		#endregion

		/// <summary>
		/// MIDI マッパーを使用する MIDI 出力ポートを開きます。
		/// </summary>
		public MidiOut()
		{
			Initialize(-1);
		}

		/// <summary>
		/// デバイス番号を指定して、MIDI 出力ポートを開きます。
		/// </summary>
		/// <param name="deviceId"></param>
		public MidiOut(int deviceId)
		{
			Initialize(deviceId);
		}

		/// <summary>
		/// デバイス名を指定して、MIDI 出力ポートを開きます。
		/// </summary>
		/// <param name="deviceName"></param>
		/// <remarks>指定されたデバイス名が見つからないときは、代わりに MIDI マッパーを開きます。</remarks>
		public MidiOut(string deviceName)
		{
			Initialize(GetDeviceId(deviceName));
		}

		/// <summary>
		/// MIDI 出力デバイスをリセットします。
		/// </summary>
		public void Reset()
		{
			MidiOutApi.MidiOutReset(this.midiOutHandle).Throw();
		}

		#region MIDI データ送信

		/// <summary>
		/// MIDI メッセージを送信します。
		/// </summary>
		/// <param name="message">送信する MIDI データ</param>
		public void SendMessage(byte[] message)
		{
			if (message.Length >= 1 && message[0] == 0xf0)
			{
				// エクスクルーシブメッセージ送信
				var hdr = new MidiHeader();
				var hdrSize = (uint)Marshal.SizeOf(hdr);

				var dataHandle = GCHandle.Alloc(message, GCHandleType.Pinned);

				try
				{
					hdr.lpData = dataHandle.AddrOfPinnedObject();
					hdr.dwBufferLength = (uint)message.Length;
					hdr.dwFlags = MidiHeaderFlags.None;

					MidiOutApi.MidiOutPrepareHeader(this.midiOutHandle, ref hdr, hdrSize).MidiOutThrow();

					while ((hdr.dwFlags & MidiHeaderFlags.Prepared) != MidiHeaderFlags.Prepared)
						Thread.Sleep(1);

					MidiOutApi.MidiOutLongMsg(this.midiOutHandle, ref hdr, hdrSize).MidiOutThrow();

					while ((hdr.dwFlags & MidiHeaderFlags.Done) != MidiHeaderFlags.Done)
						Thread.Sleep(1);

					MidiOutApi.MidiOutUnprepareHeader(this.midiOutHandle, ref hdr, hdrSize).MidiOutThrow();
				}
				finally
				{
					dataHandle.Free();
				}
			}
			else if (message.Length >= 1 && message.Length <= 3)
			{
				// ショートメッセージ
				var send = 0u;
				for (var i = 0; i < message.Length; i++)
				{
					send |= ((uint)message[i]) << (i * 8);
				}

				MidiOutApi.MidiOutShortMsg(this.midiOutHandle, send);
			}

			//if (message.Length >= 1 && message[0] == 0xf0)
			//{
			//	// エクスクルーシブメッセージ
			//	var nRet = 0;
			//	var currentHeader = new MidiHeader();
			//	for (var i = 0; i < this.systemExclusiveNumber; i++)
			//	{
			//		if (this.sysExHeader[i].lpData == IntPtr.Zero)
			//		{
			//			currentHeader = this.sysExHeader[i];
			//			break;
			//		}

			//		// ヘッダー領域が全て埋まっていた
			//		if (i >= this.systemExclusiveMaxSize)
			//			throw new MidiOutException();
			//	}
			//		// ヘッダーに MIDI メッセージをコピー
			//		var dataHandle = GCHandle.Alloc(message, GCHandleType.Pinned);
			//	try
			//	{
			//		currentHeader.dwBufferLength = (uint)Math.Min(message.Length, this.systemExclusiveMaxSize);
			//		currentHeader.lpData = dataHandle.AddrOfPinnedObject();
			//		currentHeader.dwBytesRecorded = (uint)Math.Min(message.Length, this.systemExclusiveMaxSize);

			//		MidiOutApi.MidiOutPrepareHeader(this.midiOutHandle, ref currentHeader, (uint)Marshal.SizeOf(currentHeader))
			//				.MidiOutThrow();

			//	}
			//	finally
			//	{

			//	}
			//}
		}

		#endregion

		private int GetDeviceId(string deviceName)
		{
			var search = MidiOut.MidiDevices.Select((s, i) => new {Id = i, Value = s}).Where(w => w.Value.Name == deviceName).ToArray();
			if (search.Any())
			{
				var id = search.First();
				return id.Id;
			}

			return -1;
		}

		private void Initialize(int deviceId)
		{
			var type = MidiPortOpenFlags.FunctionCallback;
			var handle = new MidiOutProc(MidiOutCallback);

			var instance = Marshal.GetFunctionPointerForDelegate(handle);

			if (Environment.Is64BitProcess)
				MidiOutApi.MidiOutOpenLong(ref this.midiOutHandle, (uint)deviceId, handle, (ulong)instance, type).MidiOutThrow();
			else
				MidiOutApi.MidiOutOpen(ref this.midiOutHandle, (uint)deviceId, handle, (uint)instance, type).MidiOutThrow();

			this.deviceId = deviceId;
			this.sysExHeader = new MidiHeader[this.systemExclusiveNumber];
		}

		/// <summary>
		/// MIDI 出力デバイスを閉じます。
		/// </summary>
		public void Dispose()
		{
			if (this.IsDisposed)
				throw new InvalidOperationException("既にこのクラスのインスタンスは破棄されています。");

			MidiOutApi.MidiOutClose(this.midiOutHandle).MidiOutThrow();

			this.IsDisposed = true;
		}
	}
}
