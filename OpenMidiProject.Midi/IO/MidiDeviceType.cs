﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OpenMidiProject.Midi.IO
{
	/// <summary>
	/// MIDI デバイスの種類を定義します。
	/// </summary>
	public enum MidiDeviceType
	{
		/// <summary>
		/// ハードウェア シンセサイザー。
		/// </summary>
		Hardware = 1,
		/// <summary>
		/// シンセサイザー。
		/// </summary>
		Synthesizer = 2,
		/// <summary>
		/// 矩形波シンセサイザー。
		/// </summary>
		SquareSynthesizer=3,
		/// <summary>
		/// FM 音源シンセサイザー。
		/// </summary>
		FmSynth=4,
		/// <summary>
		/// MIDI マッパー。
		/// </summary>
		MidiMapper=5,
		/// <summary>
		/// ウェーブテーブル シンセサイザー。
		/// </summary>
		Wabetable=6,
		/// <summary>
		/// ソフトウェア シンセサイザー。
		/// </summary>
		SoftwareSynthesizer=7
	}
}
