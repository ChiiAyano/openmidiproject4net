﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenMidiProject.Midi.Api;

namespace OpenMidiProject.Midi
{
	public static class Extensions
	{
		public static void Throw(this MmResult result)
		{
			switch (result)
			{
				case MmResult.NoError:
					return;
				case MmResult.BadDeviceId:
					throw new ArgumentOutOfRangeException();
				case MmResult.NoDriver:
					throw new InvalidOperationException();
				case MmResult.NoMemory:
					throw new InvalidOperationException();
				case MmResult.InvalidParameter:
					throw new InvalidOperationException();
			}
		}

		public static void MidiOutThrow(this MmResult result)
		{
			if (result == MmResult.NoError)
				return;

			throw new MidiOutException(result);
		}
	}
}
