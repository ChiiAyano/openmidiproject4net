﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenMidiProject.Midi.Api;

namespace OpenMidiProject.Midi
{
	internal static class ExceptionHelper
	{
		public static string GetMidiOurError(MmResult result)
		{
			var message = new StringBuilder(256);
			MidiOutApi.MidiOutGetErrorText(result, message, 256);

			if (string.IsNullOrWhiteSpace(message.ToString()))
				message = new StringBuilder(Enum.GetName(typeof(MmResult), result));

			return message.ToString();
		}
	}

	public class MidiOutException : Exception
	{
		public MidiOutException()
			:base("マルチメディア エラーが発生しました。")
		{
		}

		public MidiOutException(MmResult result)
			: base(ExceptionHelper.GetMidiOurError(result))
		{
		}

		public MidiOutException(string message)
			: base(message)
		{
		}
	}
}
